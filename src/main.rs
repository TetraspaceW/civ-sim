use ansi_term::ANSIGenericString;
use ansi_term::Colour::{Black, Blue, Fixed, Green, Red, White, Yellow};
use rand::{thread_rng, Rng};

fn main() {
    let mut world = World::generate();

    for _ in 0..2000 {
        world = world.turn();
    }

    world.present();
}

#[derive(Clone)]
pub struct World {
    data: Planet,
}

impl World {
    pub fn generate() -> World {
        World {
            data: PlanetGenerator::generate_random(12),
        }
    }

    pub fn present(&self) {
        println!("# Planet:");
        self.data.present();
    }

    pub fn turn(&self) -> World {
        let trade_blocs = self.get_trade_blocs();
        let updated_trade_blocs = trade_blocs
            .into_iter()
            .map(|trade_bloc| trade_bloc.turn())
            .collect::<Vec<TradeBloc>>();

        let mut new_world = self.clone();
        for trade_bloc in updated_trade_blocs {
            for trade_tile in trade_bloc.members {
                new_world.insert_at_address(trade_tile.base_tile, trade_tile.address);
            }
        }

        new_world
    }

    pub fn get_trade_blocs(&self) -> Vec<TradeBloc> {
        let mut trade_blocs = Vec::new();
        let (height, width) = self.data.map_size();
        for y in 0..height {
            for x in 0..width {
                trade_blocs.push(TradeBloc {
                    members: vec![TradeTile {
                        address: Address { x, y },
                        base_tile: self.data.tiles[y][x].clone(),
                        economic_innovations: 0.0,
                        technological_innovations: 0.0,
                    }],
                })
            }
        }
        trade_blocs
    }

    fn _get_at_address(&self, address: Address) -> &Tile {
        return &self.data.tiles[address.y][address.x];
    }

    fn insert_at_address(&mut self, tile: Tile, address: Address) {
        self.data.tiles[address.y][address.x] = tile
    }
}

#[derive(Clone)]
pub struct TradeBloc {
    members: Vec<TradeTile>,
}

impl TradeBloc {
    fn from_tiles(tiles: Vec<TradeTile>) -> TradeBloc {
        TradeBloc { members: tiles }
    }

    pub fn turn(&self) -> TradeBloc {
        let mut new_trade_bloc = Self::from_tiles(vec![]);
        let old_members = self.members.clone();
        for mut trade_tile in old_members {
            let innovations = (trade_tile.base_tile.economy / 8.0).max(1.0);

            match thread_rng().gen_range(0..6) {
                0..=1 => trade_tile.economic_innovations += innovations,
                2 => trade_tile.economic_innovations -= innovations,
                3..=4 => trade_tile.technological_innovations += innovations * 4.0,
                5 => trade_tile.technological_innovations -= innovations * 4.0,
                _ => {
                    panic!("d6 for tile updates rolled above 6")
                }
            }
            new_trade_bloc.members.push(trade_tile)
        }

        // now I guess each tile trades among themselves? but who can be bothered with that
        // let's just let them all keep their own innovations for now and add them purely to stock

        let bloc_peak_tech = &new_trade_bloc.peak_tech();

        for trade_tile in &mut new_trade_bloc.members {
            // check whether the bloc can do anything on this tile
            let base_tile = &mut trade_tile.base_tile;
            let bloc_compatible = base_tile.terrain.bloc_has_required_tech(bloc_peak_tech);

            if bloc_compatible {
                // improve technology
                trade_tile.invest_tech_points();
                // Grow the economy
                trade_tile.invest_econ_points();
            }
        }

        new_trade_bloc
    }

    pub fn peak_tech(&self) -> Technology {
        return self.members.clone().into_iter().fold(
            Technology::at_level(TechLevel::Zero),
            |accum, item| {
                let item_tech = item.base_tile.technology;
                Technology {
                    philosophical: accum.philosophical.max(item_tech.philosophical),
                    infrastructural: accum.infrastructural.max(item_tech.infrastructural),
                    energetic: accum.energetic.max(item_tech.energetic),
                    economic: accum.economic.max(item_tech.economic),
                }
            },
        );
    }
}

trait TradeEntity {
    fn invest_tech_points(&mut self);
    fn invest_econ_points(&mut self);
}

#[derive(Clone)]
pub struct TradeTile {
    // Probably I just want these to be trade entities, though that means that they'd all have to
    // have addresses and then the world could map addresses back to their location and insert them
    // back in
    address: Address,
    base_tile: Tile,
    economic_innovations: f64,
    technological_innovations: f64,
}

impl TradeEntity for TradeTile {
    fn invest_tech_points(&mut self) {
        self.base_tile
            .technology
            .invest_points(self.technological_innovations);
        self.technological_innovations = 0.0;
    }

    fn invest_econ_points(&mut self) {
        let base_tile = &mut self.base_tile;
        let economy_before = base_tile.economy;
        base_tile.economy =
            (base_tile.economy + self.economic_innovations).clamp(1.0, base_tile.max_economy());

        self.economic_innovations -= base_tile.economy - economy_before;

        // invest the rest in tile improvements
        base_tile.infrastructure = (base_tile.infrastructure + self.economic_innovations)
            .clamp(0.0, base_tile.max_infrastructure());
        self.economic_innovations = 0.0;
    }
}

#[derive(Clone)]
pub struct Planet {
    pub tiles: Vec<Vec<Tile>>,
}

impl Planet {
    pub fn present(&self) {
        println!("## Terrain:");
        for row in &self.tiles {
            for tile in row {
                print!("{}", tile.terrain.terrain.icon())
            }
            println!()
        }
        println!();
        println!("## Economy:");
        for row in &self.tiles {
            for tile in row {
                let inf_val = (tile.economy as f64).log10().floor() as u64;
                let val = match inf_val {
                    0 => Black.paint(" "),
                    1 => Red.paint(inf_val.to_string()),
                    2 => Yellow.paint(inf_val.to_string()),
                    3 => Green.paint(inf_val.to_string()),
                    _ => Blue.paint(inf_val.to_string()),
                };
                print!("{}", val)
            }
            println!()
        }
    }

    pub fn map_size(&self) -> (usize, usize) {
        (self.tiles.len(), self.tiles.first().unwrap().len())
    }
}

#[derive(Clone, Debug)]
pub struct Tile {
    pub terrain: TileTerrain,
    pub economy: f64,
    pub technology: Technology,
    pub infrastructure: f64,
}

impl Tile {
    fn default() -> Tile {
        Tile {
            terrain: TileTerrain {
                terrain: TerrainType::Water,
                resource: None,
            },
            economy: 0.0,
            technology: Technology::at_level(TechLevel::Zero),
            infrastructure: 0.0,
        }
    }

    fn max_economy(&self) -> f64 {
        self.technology.economic
    }

    fn max_infrastructure(&self) -> f64 {
        self.technology.infrastructural
    }
}

#[derive(Clone, Debug)]
pub struct Technology {
    philosophical: f64,
    infrastructural: f64,
    energetic: f64,
    economic: f64,
}

impl Technology {
    fn at_level(level: TechLevel) -> Technology {
        Technology {
            philosophical: level.value(),
            infrastructural: level.value(),
            energetic: level.value(),
            economic: level.value(),
        }
    }

    // would like to decouple this from the parent tile
    fn invest_points(&mut self, points: f64) {
        let worst_technology = self
            .infrastructural
            .min(self.energetic)
            .min(self.philosophical)
            .min(self.economic);

        let new_value =
            |old_value: f64| (old_value + (worst_technology / old_value) * points).max(1.0);

        match thread_rng().gen_range(0..4) {
            0 => self.philosophical = new_value(self.philosophical),
            1 => self.energetic = new_value(self.energetic),
            2 => self.economic = new_value(self.economic),
            3 => self.infrastructural = new_value(self.infrastructural),
            _ => {
                panic!("1d4 rolled to pick which tech school to enhance rolled above 4")
            }
        }
    }
}

pub enum TechLevel {
    Zero,
    Animal,
    Paleolithic,
    Mesolithic,
    Neolithic,
    Iron,
    Mercantile,
    Industrial,
    Atomic,
    Information,
    Nanotechnological,
}

impl TechLevel {
    fn value(&self) -> f64 {
        return match self {
            TechLevel::Paleolithic => 1,
            TechLevel::Mesolithic => 10,
            TechLevel::Neolithic => 100,
            TechLevel::Iron => 1_000,
            TechLevel::Mercantile => 10_000,
            TechLevel::Industrial => 100_000,
            TechLevel::Atomic => 1_000_000,
            TechLevel::Information => 10_000_000,
            TechLevel::Nanotechnological => 100_000_000,
            TechLevel::Zero => 0,
            TechLevel::Animal => 0,
        } as f64;
    }
}

#[derive(Clone, Debug)]
pub struct TileTerrain {
    pub terrain: TerrainType,
    pub resource: Option<Resource>,
}

impl TileTerrain {
    fn bloc_has_required_tech(&self, bloc_tech: &Technology) -> bool {
        bloc_tech.infrastructural >= self.required_bloc_tech()
    }

    fn required_bloc_tech(&self) -> f64 {
        match self.terrain {
            TerrainType::Water => TechLevel::Nanotechnological,
            _ => TechLevel::Animal,
        }
        .value()
    }
}

#[derive(PartialEq, Clone, Debug)]
pub enum TerrainType {
    SnowyMountain,
    Mountain,
    Grassland,
    Desert,
    Forest,
    Jungle,
    Water,
}

impl TerrainType {
    fn icon(&self) -> ANSIGenericString<str> {
        match self {
            TerrainType::SnowyMountain => White.paint("^"),
            TerrainType::Mountain => Fixed(8).paint("^"),
            TerrainType::Grassland => Green.paint("#"),
            TerrainType::Desert => Yellow.paint("."),
            TerrainType::Forest => Green.paint("~"),
            TerrainType::Jungle => Green.paint("*"),
            TerrainType::Water => Blue.paint("≈"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Resource {}

pub struct PlanetGenerator {}

impl PlanetGenerator {
    pub fn generate_random(r: usize) -> Planet {
        Self::generate_random_cylindrical_planet(r * 2, r)
    }

    fn generate_random_cylindrical_planet(width: usize, height: usize) -> Planet {
        let mut rng = thread_rng();

        // Generate random elevations for each point on the planet.
        let mut elevations = vec![vec![0.0; width]; height];
        for y in 0..height {
            for x in 0..width {
                elevations[y][x] = rng.gen_range(0.0..1.0);
            }
        }

        // Smooth the elevations using a simple moving average filter.
        let mut smoothed_elevations = vec![vec![0.0; width]; height];
        for y in 0..height {
            for x in 0..width {
                let mut sum = 0.0;
                let mut count = 0;

                for dy in -1..=1 {
                    for dx in -1..=1 {
                        let nx = (x as isize + dx) as isize;
                        let ny = (y as isize + dy) as isize;

                        if nx >= 0
                            && nx < width.try_into().unwrap()
                            && ny >= 0
                            && ny < height.try_into().unwrap()
                        {
                            sum += elevations[ny as usize][nx as usize];
                            count += 1;
                        }
                    }
                }

                smoothed_elevations[y][x] = sum / count as f32;
            }
        }

        // Use the smoothed elevations to determine the terrain type for each point on the planet.
        let mut terrain = vec![vec![TerrainType::Water; width]; height];
        let water_level = 0.5;
        for y in 0..height {
            for x in 0..width {
                let elevation = smoothed_elevations[y][x];
                let temperature = 0.5 * (1.0 - y as f32 / height as f32);
                let moisture = 0.5 * (1.0 - x as f32 / width as f32);

                if elevation >= water_level {
                    terrain[y][x] =
                        PlanetGenerator::determine_terrain(elevation, temperature, moisture);
                }
            }
        }

        Planet {
            tiles: (PlanetGenerator::terrain_to_tile_map(terrain)),
        }
    }

    fn determine_terrain(elevation: f32, temperature: f32, moisture: f32) -> TerrainType {
        let is_high_elevation = elevation > 0.6;
        let is_cold_temperature = temperature < 0.25;
        let is_wet_conditions = moisture > 0.25;

        let terrain_grid = [
            // low elevation
            [
                // cold temperature
                [TerrainType::Forest, TerrainType::Grassland],
                // warm temperature
                [TerrainType::Jungle, TerrainType::Desert],
            ],
            // high elevation
            [
                // cold temperature
                [TerrainType::SnowyMountain, TerrainType::Mountain],
                // warm temperature
                [TerrainType::Grassland, TerrainType::Desert],
            ],
        ];

        let terrain_index = is_high_elevation as usize;
        let temperature_index = is_cold_temperature as usize;
        let moisture_index = is_wet_conditions as usize;

        terrain_grid[terrain_index][temperature_index][moisture_index].clone()
    }

    fn terrain_to_tile_map(terrain: Vec<Vec<TerrainType>>) -> Vec<Vec<Tile>> {
        let height = terrain.len();
        let width = terrain[0].len();

        let mut tiles = vec![vec![Tile::default(); width]; height];

        for y in 0..height {
            for x in 0..width {
                let terrain_type = &terrain[y][x];

                let mut tile = &mut tiles[y][x];
                tile.terrain = TileTerrain {
                    terrain: terrain_type.clone(),
                    resource: None,
                };

                match terrain_type {
                    TerrainType::Water => {
                        tile.economy = 0.0;
                        tile.technology = Technology::at_level(TechLevel::Animal);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::Desert => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::Grassland => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::Forest => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::Jungle => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::SnowyMountain => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                    TerrainType::Mountain => {
                        tile.economy = 1.0;
                        tile.technology = Technology::at_level(TechLevel::Paleolithic);
                        tile.infrastructure = 0.0;
                    }
                }

                tiles[y][x] = tile.clone();
            }
        }

        tiles
    }
}

pub struct Utils {}

impl Utils {
    fn smooth(x: usize, y: usize, first_pass: Vec<Vec<f64>>) -> Vec<Vec<f64>> {
        let mut smoothed: Vec<Vec<f64>> = Vec::new();

        for y_i in 0..y {
            let mut smoothed_row: Vec<f64> = Vec::new();
            for x_i in 0..x {
                let x_left = ((x_i as isize) - 1).rem_euclid(x as isize) as usize;
                let x_right = (x_i as isize + 1).rem_euclid(x as isize) as usize;
                let y_above = ((y_i as isize) - 1).clamp(0, (y - 1) as isize) as usize;
                let y_below = ((y_i as isize) + 1).clamp(0, (y - 1) as isize) as usize;

                smoothed_row.push(
                    (first_pass[y_i][x_i]
                        + first_pass[y_i][x_left]
                        + first_pass[y_i][x_right]
                        + first_pass[y_above][x_i]
                        + first_pass[y_below][x_i])
                        / 5_f64,
                );
            }
            smoothed.push(smoothed_row)
        }
        smoothed
    }
}

#[derive(Clone)]
pub struct Address {
    x: usize,
    y: usize,
}
